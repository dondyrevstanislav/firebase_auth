module.exports = {
    env: {
        browser: true,
        es2021: true,
        jest: true,
    },
    extends: ['plugin:react/recommended', 'standard'],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2021,
        sourceType: 'module',
    },
    plugins: ['react'],
    rules: {
        semi: [2, 'always'],
        indent: [0, 4],
        'space-before-function-paren': [
            'error',
            { anonymous: 'always', named: 'never' },
        ],
        'multiline-ternary': ['off'],
        quotes: [
            'error',
            'single',
            {
                allowTemplateLiterals: true,
                avoidEscape: true,
            },
        ],
        'spaced-comment': ['error', 'always'],
        'react/display-name': 'off',
        'comma-dangle': ['error', 'always-multiline'],
        'react/react-in-jsx-scope': 'off',
    },
};
