module.exports = {
    trailingComma: 'all',
    tabWidth: 4,
    semi: true,
    'jsx-single-quote': true,
    singleQuote: true,
    arrowParens: 'always',
};
