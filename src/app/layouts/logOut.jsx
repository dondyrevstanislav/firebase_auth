// eslint-disable-next-line react/prop-types
const LogOut = ({ handleClick }) => {
    return (
        <>
            <h1>You are not authorized</h1>
            <button onClick={handleClick}>Login</button>
        </>
    );
};

export default LogOut;
