// eslint-disable-next-line react/prop-types
const LogIn = ({ handleClick }) => {
    return (
        <>
            <h1>You are logged in</h1>
            <button onClick={handleClick}>Logout</button>
        </>
    );
};

export default LogIn;
