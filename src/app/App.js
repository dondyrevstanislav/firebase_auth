import { useState } from 'react';
import LogIn from './layouts/logIn';
import LogOut from './layouts/logOut';

function App() {
    const [isAuth, setIsAuth] = useState(false);

    const toggleAuth = () => {
        setIsAuth((prevState) => !prevState);
    };

    return (
        <div>
            {isAuth ? (
                <LogIn handleClick={toggleAuth}></LogIn>
            ) : (
                <LogOut handleClick={toggleAuth}></LogOut>
            )}
        </div>
    );
}

export default App;
